package jp.co.noticeboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.noticeboard.dto.UserDto;
import jp.co.noticeboard.dto.factory.BranchDtoFactory;
import jp.co.noticeboard.dto.factory.PositionDtoFactory;
import jp.co.noticeboard.entity.factory.UserFactory;
import jp.co.noticeboard.mapper.BranchesMapper;
import jp.co.noticeboard.mapper.PositionsMapper;
import jp.co.noticeboard.mapper.UsersMapper;

@Service
public class SignupService {
	@Autowired
	private BranchesMapper branchesMapper;
	@Autowired
	private PositionsMapper positionsMapper;
	@Autowired
	private UsersMapper usersMapper;
	@Autowired
	private BranchDtoFactory branchDtoFactory;
	@Autowired
	private PositionDtoFactory positionDtoFactory;
	@Autowired
	private UserFactory userFactory;


	public void registUser(UserDto user) {
		usersMapper.regist(userFactory.createEncrypt(user));
	}

	public List
}
