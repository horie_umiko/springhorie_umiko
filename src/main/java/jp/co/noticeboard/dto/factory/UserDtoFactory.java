package jp.co.noticeboard.dto.factory;

import org.springframework.stereotype.Component;

import jp.co.noticeboard.dto.UserDto;
import jp.co.noticeboard.form.SignupForm;

@Component
public class UserDtoFactory {
	public UserDto create(SignupForm form) {
		return new UserDto(
				null,
				form.getLoginId(),
				form.getPassword(),
				form.getAccountName(),
				form.getBranchId(),
				form.getPositionId(),
				1);
	}
}
