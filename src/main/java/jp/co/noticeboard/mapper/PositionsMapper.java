package jp.co.noticeboard.mapper;

import java.util.List;

import javax.swing.text.Position;

import org.springframework.stereotype.Component;

@Component
public interface PositionsMapper {
	List<Position> getAll();
}
