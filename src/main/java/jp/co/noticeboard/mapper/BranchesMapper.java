package jp.co.noticeboard.mapper;

import java.util.List;

import org.dom4j.Branch;
import org.springframework.stereotype.Component;

@Component
public interface BranchesMapper {
	List<Branch> getAll();
}
