package jp.co.noticeboard.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import jp.co.noticeboard.entity.User;

@Component
public interface UsersMapper {
	User getUser(@Param("loginId") String loginId, @Param("password") String password);


	void regist(User user);

}
